package in.finezza.correction.service.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BsaController {

	@RequestMapping("/bsa")
	public String testBsaController() {
		return "BSA Controller Working";
	}
}
