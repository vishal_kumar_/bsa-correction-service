package in.finezza.correction.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorrectionServiceMain {

	public static void main(String[] args) {
		SpringApplication.run(CorrectionServiceMain.class, args);
	}

}
